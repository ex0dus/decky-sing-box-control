import { configureStore } from "@reduxjs/toolkit";
import uiSliceReducer from "./uiSlice";
// import { logger } from './logger';

export const store = configureStore({
  reducer: {
    ui: uiSliceReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat([
      // logger
    ]),
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
