import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import { RootState } from ".";
// import type { RootState } from './store';

type UiStateType = {
  initialToggleState: boolean;
};

// Define the initial state using that type
const initialState: UiStateType = {
  initialToggleState: false,
};

export const uiSlice = createSlice({
  name: "ui",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    setToggleState: (state, action: PayloadAction<boolean>) => {
      state.initialToggleState = action.payload;
    },
  },
});

export const getToggleState = (state: RootState) => state.ui.initialToggleState;
export const { setToggleState } = uiSlice.actions;
export default uiSlice.reducer;
