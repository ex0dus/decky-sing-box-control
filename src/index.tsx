import {
  ButtonItem,
  definePlugin,
  DialogButton,
  Menu,
  MenuItem,
  PanelSection,
  PanelSectionRow,
  Navigation,
  ServerAPI,
  showContextMenu,
  staticClasses,
  ToggleField,
} from "decky-frontend-lib";
import { useEffect, useState, VFC } from "react";
import { FaShip } from "react-icons/fa";

import logo from "../assets/logo.png";
import {
  createServerApiHelpers,
  logInfo,
  saveServerApi,
} from "./utils/backend";
import { MainToggle } from "./components/MainToggle";
import { Provider } from "react-redux";
import { store } from "./store";
import { setToggleState } from "./store/uiSlice";
import { DashboardBtn } from "./components/DashboardBtn";

// interface AddMethodArgs {
//   left: number;
//   right: number;
// }

const Content: VFC<{ serverApi: ServerAPI }> = ({ serverApi }) => {
  return (
    <div>
      <MainToggle />
      <DashboardBtn />
    </div>
  );
};

function AppContainer({ serverApi }: { serverApi: ServerAPI }) {
  useEffect(() => {
    // Updated every render, ensure latest toggle state
    const { getServiceInfo } = createServerApiHelpers(serverApi);

    getServiceInfo().then((res) => {
      logInfo(res);
      if (res.success) {
        const result = res.result || false;

        console.log(result);
        store.dispatch(setToggleState(result));
      }
    });
  }, []);

  return (
    <Provider store={store}>
      <Content serverApi={serverApi} />
    </Provider>
  );
}

const DeckyPluginRouterTest: VFC = () => {
  return (
    <div style={{ marginTop: "50px", color: "white" }}>
      Hello World!
      <DialogButton onClick={() => Navigation.NavigateToLibraryTab()}>
        Go to Library
      </DialogButton>
    </div>
  );
};

export default definePlugin((serverApi: ServerAPI) => {
  saveServerApi(serverApi);

  const { getServiceInfo } = createServerApiHelpers(serverApi);

  getServiceInfo().then((res) => {
    logInfo(res);
    if (res.success) {
      const result = res.result || false;

      console.log(result);
      store.dispatch(setToggleState(result));
    }
  });

  serverApi.routerHook.addRoute("/service-toggle", DeckyPluginRouterTest, {
    exact: true,
  });

  return {
    title: <div className={staticClasses.Title}>Example Plugin</div>,
    content: <AppContainer serverApi={serverApi} />,
    icon: <FaShip />,
    onDismount() {
      serverApi.routerHook.removeRoute("/service-toggle");
    },
  };
});
