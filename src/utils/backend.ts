import { ServerAPI } from "decky-frontend-lib";

export enum ServerAPIMethods {
  TOGGLE_SERVICE = "toggle_service",
  GET_SERVICE_INFO = "get_service_info",
  LOG_INFO = "log_info",
}

let serverApi: undefined | ServerAPI;

export const saveServerApi = (s: ServerAPI) => {
  serverApi = s;
};
export const getServerApi = () => {
  return serverApi;
};

const createLogInfo = (serverAPI: ServerAPI) => async (info: any) => {
  await serverAPI.callPluginMethod(ServerAPIMethods.LOG_INFO, {
    info: JSON.stringify(info),
  });
};

const createGetServiceInfo = (serverAPI: ServerAPI) => async () => {
  return await serverAPI.callPluginMethod<unknown, boolean>(
    ServerAPIMethods.GET_SERVICE_INFO,
    {}
  );
};

const createToggleService =
  (serverAPI: ServerAPI) => async (toggle: boolean) => {
    return await serverAPI.callPluginMethod(ServerAPIMethods.TOGGLE_SERVICE, {
      toggle,
    });
  };

export const logInfo = (info: any) => {
  const s = getServerApi();
  s &&
    s.callPluginMethod(ServerAPIMethods.LOG_INFO, {
      info: JSON.stringify(info),
    });
};

export const createServerApiHelpers = (serverAPI: ServerAPI) => {
  return {
    toggleService: createToggleService(serverAPI),
    logInfo: createLogInfo(serverAPI),
    getServiceInfo: createGetServiceInfo(serverAPI),
  };
};
