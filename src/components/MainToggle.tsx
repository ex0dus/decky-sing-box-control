import {
  ButtonItem,
  definePlugin,
  DialogButton,
  Menu,
  MenuItem,
  PanelSection,
  PanelSectionRow,
  Router,
  ServerAPI,
  showContextMenu,
  staticClasses,
  ToggleField,
} from "decky-frontend-lib";
import { useEffect, useState, VFC } from "react";
import { FaShip } from "react-icons/fa";
import { createServerApiHelpers, getServerApi } from "../utils/backend";
import { useAppDispatch, useAppSelector } from "../store/hooks";
import { getToggleState, setToggleState } from "../store/uiSlice";

export function MainToggle() {
  const checked = useAppSelector(getToggleState);
  const dispatch = useAppDispatch();
  const [err, setErr] = useState<any>(undefined);

  const serverApi = getServerApi();
  if (!serverApi) return null;
  const { toggleService } = createServerApiHelpers(serverApi);

  return (
    <PanelSection title="Service Toggle">
      <PanelSectionRow>
        <ToggleField
          label="Enable"
          checked={checked}
          onChange={async (_checked) => {
            try {
              const result = await toggleService(Boolean(_checked));

              const { getServiceInfo } = createServerApiHelpers(serverApi);
              const serviceInfoRes = await getServiceInfo();
              if (serviceInfoRes.success) {
                const result = serviceInfoRes.result || false;
                dispatch(setToggleState(result));
              }
            } catch (e: any) {
              console.log(e);
              setErr(e);
            }
          }}
        ></ToggleField>
        <div>{err?.message ?? "No Error"}</div>
      </PanelSectionRow>
    </PanelSection>
  );
}
