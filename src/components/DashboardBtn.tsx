import {
  ButtonItem,
  Navigation,
  PanelSectionRow,
  Router,
} from "decky-frontend-lib";

export function DashboardBtn() {
  return (
    <PanelSectionRow>
      <ButtonItem
        layout="below"
        onClick={() => {
          Router.CloseSideMenus();
          Navigation.NavigateToExternalWeb("http://localhost:9090/ui");
          //Router.NavigateToExternalWeb("http://127.0.0.1:9090/ui")
        }}
      >
        Open Dashboard
      </ButtonItem>
    </PanelSectionRow>
  );
}
