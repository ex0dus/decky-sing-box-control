import logging, os
import subprocess

# The decky plugin module is located at decky-loader/plugin
# For easy intellisense checkout the decky-loader code one directory up
# or add the `decky-loader/plugin` path to `python.analysis.extraPaths` in `.vscode/settings.json`
import decky_plugin

try:
    LOG_LOCATION = f"/tmp/serviceToggle.log"
    logging.basicConfig(
        level = logging.INFO,
        filename = LOG_LOCATION,
        format="[%(asctime)s | %(filename)s:%(lineno)s:%(funcName)s] %(levelname)s: %(message)s",
        filemode = 'w',
        force = True)
except Exception as e:
    logging.error(f"exception|{e}")

logger = logging.getLogger()
logger.setLevel(logging.INFO) # can be changed to logging.DEBUG for debugging issues

class Plugin:
    # Asyncio-compatible long-running code, executed in a task when the plugin is loaded
    async def _main(self):
        decky_plugin.logger.info("Hello World!")

    # A normal method. It can be called from JavaScript using call_plugin_function("method_1", argument1, argument2)
    async def get_service_info(self):
        status = subprocess.run("systemctl show -p ActiveState --value sing-box@config", shell=True, capture_output=True)
        if status.stdout.decode("utf-8").strip() == "active": 
            return True
        else: 
            return False
    
    async def toggle_service(self, toggle: bool): 
        decky_plugin.logger.info("Toggle service {}".format(toggle))
        logger.info("Toggle service {}".format(toggle))
        if toggle:
           return subprocess.check_call("systemctl start sing-box@config", shell=True)
        else:
           return subprocess.check_call("systemctl stop sing-box@config", shell=True)
    
    async def logger(self, logLevel:str, msg:str):
        msg = '[frontend] {}'.format(msg)
        match logLevel.lower():
            case 'info':      logger.info(msg)
            case 'debug':     logger.debug(msg)
            case 'warning':   logger.warning(msg)
            case 'error':     logger.error(msg)
            case 'critical':  logger.critical(msg)
    
    async def log_info(self, info):
        logging.info(info)

    # Function called first during the unload process, utilize this to handle your plugin being removed
    async def _unload(self):
        decky_plugin.logger.info("Goodbye World!")
        pass

    # Migrations that should be performed before entering `_main()`.
    async def _migration(self):
        decky_plugin.logger.info("Migrating")
        # Here's a migration example for logs:
        # - `~/.config/decky-template/template.log` will be migrated to `decky_plugin.DECKY_PLUGIN_LOG_DIR/template.log`
        decky_plugin.migrate_logs(os.path.join(decky_plugin.DECKY_USER_HOME,
                                               ".config", "decky-template", "template.log"))
        # Here's a migration example for settings:
        # - `~/homebrew/settings/template.json` is migrated to `decky_plugin.DECKY_PLUGIN_SETTINGS_DIR/template.json`
        # - `~/.config/decky-template/` all files and directories under this root are migrated to `decky_plugin.DECKY_PLUGIN_SETTINGS_DIR/`
        decky_plugin.migrate_settings(
            os.path.join(decky_plugin.DECKY_HOME, "settings", "template.json"),
            os.path.join(decky_plugin.DECKY_USER_HOME, ".config", "decky-template"))
        # Here's a migration example for runtime data:
        # - `~/homebrew/template/` all files and directories under this root are migrated to `decky_plugin.DECKY_PLUGIN_RUNTIME_DIR/`
        # - `~/.local/share/decky-template/` all files and directories under this root are migrated to `decky_plugin.DECKY_PLUGIN_RUNTIME_DIR/`
        decky_plugin.migrate_runtime(
            os.path.join(decky_plugin.DECKY_HOME, "template"),
            os.path.join(decky_plugin.DECKY_USER_HOME, ".local", "share", "decky-template"))
